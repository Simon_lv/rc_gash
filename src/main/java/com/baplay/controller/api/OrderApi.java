package com.baplay.controller.api;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baplay.utils.Configuration;
import com.baplay.utils.MD5Util;
import com.baplay.constants.Config;
import com.baplay.controller.BaseController;
import com.baplay.dto.Invoice;
import com.baplay.dto.Payment;
import com.baplay.service.InvoiceManager;
import com.baplay.service.PaymentManager;

@Controller
@Scope("request")
public class OrderApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(OrderApi.class);
	private final String KEY = "cfec303b5bb107ef5bbc72a327d30229";
//	private final String PAID = "LARGEREMIT";
	private final String CUID = "TWD";
	private final String RCODE = "0000";
	private final String PAY_STATUS = "S";
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final SimpleDateFormat sdf0 = new SimpleDateFormat("yyyyMMddHHmmss");
	
	@Autowired
	protected PaymentManager payManager;
	
	@Autowired
	protected InvoiceManager invoiceManager;
	
	/**
	 * 大額/到府
	 */
	@RequestMapping(value = "/order")
	public @ResponseBody Map<String, String> order(
			@RequestParam("orderId") String payOrderId, //orderId金流方 
			@RequestParam("uid") String uid,
			@RequestParam(value = "accName", required = false) String accName,
			@RequestParam(value = "accNo", required = false) String accNo,
			@RequestParam("money") int money,
			@RequestParam("orderTime") String orderTime,
			@RequestParam("invType") int invType,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "invName", required = false) String invName,
			@RequestParam(value = "invPhone", required = false) String invPhone,
			@RequestParam(value = "invAddress", required = false) String invAddress,
			@RequestParam(value = "invTitle", required = false) String invTitle,
			@RequestParam(value = "invTaxId", required = false) String invTaxId,
			@RequestParam(value = "channel", required = false, defaultValue="") String channel,
			@RequestParam("token") String token ) {
		Map<String, String> result = new HashMap<String, String>();
		String code = Config.RETURN_SUCCESS;
		
		StringBuffer sf = new StringBuffer();
		sf.append("============== OrderApi order ====================\n");
		sf.append("input param [orderId=").append(payOrderId).append(", accName=").append(accName).append(", accNo=").append(accNo);
		sf.append(", uid=").append(uid).append(", money=").append(money).append(", orderTime=").append(orderTime).
		append(", invType=").append(invType).append(", email=").append(email).append(", invName=").append(invName).
		append(", invPhone=").append(invPhone).append(", invAddress=").append(invAddress).
		append(", invTitle=").append(invTitle).append(", invTaxId=").append(invTaxId).append(", channel=").append(channel).
		append(", token=").append(token).append("]"); 
		
		LOG.info(sf.toString());
		try {
			if (!token.equalsIgnoreCase(MD5Util.crypt(payOrderId +uid+money+ orderTime + invType +KEY))) {
				code = Config.RETURN_TOKEN_FAIL;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				System.out.println("result="+result);
				return result;
			}
			
			if(invType == 2 && StringUtils.isBlank(email))
			{
				code = Config.RETURN_DATA_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				LOG.info("result="+result);
				return result;
			}
			
			if(invType > 0 &&
					(
							StringUtils.isBlank(invName) ||
							StringUtils.isBlank(invPhone) ||
							StringUtils.isBlank(invAddress)
					)
			) {
				code = Config.RETURN_DATA_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			if(invType == 3 &&
					(
							StringUtils.isBlank(invTitle) ||
							StringUtils.isBlank(invTaxId)
					)
			) {
				code = Config.RETURN_DATA_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			String paid = "LARGEREMIT";
			if ("DS".equalsIgnoreCase(channel)) {
				paid = "HOME";
			}
			try {
				savePayment(money, new Timestamp(sdf.parse(orderTime).getTime()), payOrderId, paid);
			} catch (DataIntegrityViolationException div) {
				div.printStackTrace();
	    	}
			saveInvoice(payOrderId, uid, accName, accNo, invType, email, invName, invPhone, invAddress, invTitle, invTaxId, new Timestamp(sdf.parse(orderTime).getTime()));
		} catch(Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}

	private Invoice saveInvoice(String orderId, String uid, String accName, String accNo, int invoiceType, String email, String invoiceName, String invoicePhone, String invoiceAddress, String invoiceTitle, String taxId, Timestamp createTime) {
		Invoice invoice = new Invoice();
		invoice.setOrderId(orderId);
		invoice.setUid(uid);
		invoice.setInvoiceType(invoiceType);
		invoice.setAccName(accName);
		invoice.setAccNo(accNo);
		invoice.setEmail(email);
		invoice.setInvoiceName(StringUtils.isNotBlank(invoiceName)?invoiceName:accName);
		invoice.setInvoicePhone(invoicePhone);
		invoice.setInvoiceAddress(invoiceAddress);
		invoice.setInvoiceTitle(invoiceTitle);
		invoice.setTaxId(taxId);
		invoice.setCreateTime(createTime);
//		invoice.setLastPayTime(createTime);
//		invoice.setStatus(status);
		invoiceManager.add(invoice);
		return invoice;
	}


	private Payment savePayment(int money, Timestamp createTime, String payOrderId, String paid) {
		Payment payment = new Payment();
		payment.setRRN(payOrderId);
		payment.setPAID(paid);
		payment.setCUID(CUID);
		payment.setAMOUNT(money);
		payment.setTXTIME(sdf0.format(createTime));
		payment.setPAY_STATUS(PAY_STATUS);
		payment.setRCODE(RCODE);
		
		payment = payManager.add(payment);
		return payment;
	}
	
	private String getTempXml(){
		StringBuffer xml = new StringBuffer("<?xml version=\"1.0\" encoding=\"utf-8\" ?><gashInReq>");
		xml.append(Configuration.getInstance().getProperty("api.gashInReq.xml"));
		xml.append("</Gash></gashInReq>");
		return xml.toString();
	}
	
	public static void main(String[] args) {
		System.out.println(MD5Util.crypt("rcpay"));
	}
}
