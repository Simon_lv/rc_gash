package com.baplay.controller.api;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baplay.constants.Config;
import com.baplay.controller.BaseController;
import com.baplay.dao.PaymentDao;
import com.baplay.dao.PaymentMycardDao;
import com.baplay.dto.Payment;
import com.baplay.dto.PaymentMycard;
import com.baplay.utils.MD5Util;
import com.google.gson.Gson;








@Controller
@Scope("request")
public class ReceivePayApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(ReceivePayApi.class);
	
	@Autowired
	private PaymentDao paymentManager;
	
	@Autowired
	private PaymentMycardDao paymentMycardManager;
	
	private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssS");
	 
	private final String KEY = "FF78648BE52A4E79513F4E70B266C62A";
	
	/**
	 * GASH
	 * @param data
	 * @param token
	 * @return
	 */
	@RequestMapping(value = "pay", produces = "application/json")
	public @ResponseBody Map pay(@RequestParam("data") String data,
			@RequestParam("token") String token)
	{	
		LOG.info("============== ReceivePayApi pay ====================");
		LOG.info("data="+data);
		LOG.info("token="+token);
		
		String code = Config.RETURN_SUCCESS;
		Map result = new HashMap();
		if (!token.equalsIgnoreCase(MD5Util.crypt(data+KEY))) {
			code = Config.RETURN_TOKEN_FAIL;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
			LOG.info("result="+result);
			return result;
		}
		try {
			Gson gson = new Gson();
			JSONObject datajson = new JSONObject(data);
	    	Payment pay = gson.fromJson(datajson.toString(), Payment.class);
	    	LOG.info("轉換後的json="+datajson.toString());
	    	
    		if (paymentManager.findByOrderId(pay.getRRN()) == null) { 
    			pay = paymentManager.add(pay);
    		} else if (pay != null) {
    			paymentManager.update(pay);
    		} else {
    			code = Config.RETURN_DATA_ERROR;
    		}	
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
			
		} catch (Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		LOG.info("result="+result);
		return result;
	}
	
	/**
	 * Mycard
	 * @param data
	 * @param token
	 * @return
	 */
	@RequestMapping(value = "mcPay", produces = "application/json")
	public @ResponseBody Map mcPay(@RequestParam("data") String data,
			@RequestParam("token") String token)
	{	
		LOG.info("============== ReceivePayApi mcPay ====================");
		LOG.info("data="+data);
		LOG.info("token="+token);
		
		String code = Config.RETURN_SUCCESS;
		Map result = new HashMap();
		if (!token.equalsIgnoreCase(MD5Util.crypt(data+KEY))) {
			code = Config.RETURN_TOKEN_FAIL;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
			LOG.info("result="+result);
			return result;
		}
		try {
			Gson gson = new Gson();
			JSONObject datajson = new JSONObject(data);
	    	PaymentMycard pay = gson.fromJson(datajson.toString(), PaymentMycard.class);
	    	LOG.info("轉換後的json="+datajson.toString());
	    	
    		if (paymentMycardManager.findByOrderId(pay.getTradeSeq()) == null) { 
    			pay = paymentMycardManager.add(pay);
    		} else if (pay != null) {
    			paymentMycardManager.update(pay);
    		} else {
    			code = Config.RETURN_DATA_ERROR;
    		}	
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
			
		} catch (Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		LOG.info("result="+result);
		return result;
	}
	/*
	 * 沒使用
	 */
	@RequestMapping(value = "payDG", produces = "application/json")
	public @ResponseBody Map payDG(@RequestParam("data") String data,
			@RequestParam("token") String token)
	{	
		LOG.info("============== ReceivePayApi payDG ====================");
		LOG.info("data="+data);
		LOG.info("token="+token);
		
		String code = Config.RETURN_SUCCESS;
		Map result = new HashMap();
		if (!token.equalsIgnoreCase(MD5Util.crypt(data+KEY))) {
			code = Config.RETURN_TOKEN_FAIL;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
			LOG.info("result="+result);
			return result;
		}
		try {
			Gson gson = new Gson();
			JSONObject datajson = new JSONObject(data);
	    	Payment pay = gson.fromJson(datajson.toString(), Payment.class);
	    	LOG.info("轉換後的json="+datajson.toString());
	    	try {
	    		pay = paymentManager.add(pay);
	    	} catch (DataIntegrityViolationException div) {
	    		
	    	}
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
			
		} catch (Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		LOG.info("result="+result);
		return result;
	}
	
	public static String unescapeUnicode(String str){
        StringBuffer b=new StringBuffer();
        Matcher m = Pattern.compile("\\\\u([0-9a-fA-F]{4})").matcher(str);
        while(m.find())
            b.append((char)Integer.parseInt(m.group(1),16));
        return b.toString();
    }
	
	public static void main(String[] arg) {
		//String json = "{\"MSG_TYPE\":\"0110\",\"PCODE\":\"300000\",\"CID\":\"C004120000712\",\"COID\":\"8100715070100040934\",\"RRN\":\"GP15070120000832\",\"CUID\":\"TWD\",\"PAID\":\"COPGAM05\",\"AMOUNT\":\"0\",\"ERPC\":\"kgKFR7i6dyqR2nJmFHJtnLbwXtk=\",\"ORDER_TYPE\":\"\",\"PAY_STATUS\":\"F\",\"PAY_RCODE\":\"3904\",\"RCODE\":\"3904\",\"ERP_ID\":\"PINHALL\",\"MID\":\"M1000412\",\"BID\":\"\",\"MEMO\":\"RC Coins\",\"PRODUCT_NAME\":\"Buy RC Coins\",\"PRODUCT_ID\":\"PINHALL\",\"USER_ACCTID\":\"8703200\",\"USER_GROUPID\":\"\",\"USER_IP\":\"223.139.49.246\",\"EXTENSION\":\"\",\"GPS_INFO\":\"\",\"TXTIME\":\"20150701000410\",\"RMSG\":\"INVALID_PIN\",\"RMSG_CHI\":\"\\u5132\\u503c\\u5bc6\\u78bc\\u932f\\u8aa4\"}";
		String json1 = "{\"ReturnCode\":\"1\",\"ReturnMsg\":\"\\u7db2\\u7ad9\\u5167\\u5bb9\\u554f\\u984c\\u8acb\\u6d3d\\u7db2\\u7ad9\\u5ba2\\u670d\\uff0c\\u82e5\\u70ba\\u4ea4\\u6613\\u554f\\u984c\\u8acb\\u64a5\\u6253(02)26510754\\u2027\",\"PayResult\":\"3\",\"FacTradeSeq\":\"8100716062714201518\",\"PaymentType\":\"INGAME\",\"Amount\":\"150\",\"Currency\":\"TWD\",\"MyCardTradeNo\":\"MCARCC0000000754\",\"MyCardType\":\"2\",\"PromoCode\":\"A0000\",\"SerialId\":\"\",\"AuthCode\":\"D7BA44539FF9259747CFA3794ADB38A03C0F939245CB8B110F001EE38553BAF8A55D8FEF91457AD99DF872F5950756B284DBDD109B90FA822576F5E386DD360B\",\"TradeSeq\":\"KDS1606270000200\",\"CustomerId\":\"5148647\",\"FinishTime\":\"2016-06-27 14:20:46\"}";
			
		JSONObject jsonObj = new JSONObject(json1); 
		System.out.println(jsonObj.toString());
		Gson gson = new Gson();
		PaymentMycard pay = gson.fromJson(jsonObj.toString(), PaymentMycard.class);
		System.out.println("pay="+pay);
	}
		
	
}
