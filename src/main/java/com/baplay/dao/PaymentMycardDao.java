package com.baplay.dao;

import java.sql.Timestamp;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.baplay.dto.PaymentMycard;



/**
 * 儲值紀錄
 * @version 1.0
 * */
@Repository
public class PaymentMycardDao extends BaseDao {
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public PaymentMycardDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}

	public PaymentMycard findById(long id) {
		PaymentMycard smsRecord = (PaymentMycard) queryForObject(dsMnMainQry,"SELECT * FROM payment_mycard where id = ?",new Object[]{id}, PaymentMycard.class);
		return smsRecord;
	}
	
	public PaymentMycard findByOrderId(String tradeSeq) {
		PaymentMycard smsRecord = (PaymentMycard) queryForObject(dsMnMainQry,"SELECT * FROM payment_mycard where TradeSeq = ?",new Object[]{tradeSeq}, PaymentMycard.class);
		return smsRecord;
	}
	
	public PaymentMycard add(final PaymentMycard pay) {
		final String sql = 
				"INSERT INTO payment_mycard("
				+"ReturnCode, ReturnMsg, PayResult, PaymentType, Amount, "
				+"Currency, MycardTradeNo, MycardType, PromoCode, SerialId, "
				+"AuthCode, TradeSeq, FacTradeSeq, CustomerId, FinishTime) values "+
				"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		return (PaymentMycard) addForObject(dsMnMainUpd, sql, pay, new Object[] { 
				pay.getReturnCode(), pay.getReturnMsg(), pay.getPayResult(), pay.getPaymentType(), pay.getAmount(),
				pay.getCurrency(), pay.getMyCardTradeNo(), pay.getMyCardType(), pay.getPromoCode(), pay.getSerialId(),
				pay.getAuthCode(), pay.getTradeSeq(), pay.getFacTradeSeq(), pay.getCustomerId(), pay.getFinishTime()
		});
	}

	public int update(final PaymentMycard pay) {
		final String sql = 
				"UPDATE payment_mycard SET "
				+"ReturnCode = ?, ReturnMsg = ?, PayResult = ?, PaymentType = ?, "
				+"Amount = ?, Currency = ?, MyCardTradeNo = ?, MyCardType = ?, "
				+"PromoCode = ?, SerialId = ?, AuthCode = ?, FacTradeSeq = ?, CustomerId = ?, "
				+"FinishTime = ? WHERE TradeSeq=?";
		
		return updateForObject(dsMnMainUpd, sql, new Object[] { 
				pay.getReturnCode(), pay.getReturnMsg(), pay.getPayResult(), pay.getPaymentType(), 
				pay.getAmount(), pay.getCurrency(), pay.getMyCardTradeNo(), pay.getMyCardType(),
				pay.getPromoCode(), pay.getSerialId(), pay.getAuthCode(), pay.getFacTradeSeq(), pay.getCustomerId(), 
				pay.getFinishTime(), pay.getTradeSeq()
		});
	}
	
	
}
