package com.baplay.dao;

import java.sql.Timestamp;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.baplay.dto.Payment;



/**
 * 儲值紀錄
 * @version 1.0
 * */
@Repository
public class PaymentDao extends BaseDao {
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public PaymentDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}

	public Payment findById(long id) {
		Payment smsRecord = (Payment) queryForObject(dsMnMainQry,"SELECT * FROM payment where id = ?",new Object[]{id}, Payment.class);
		return smsRecord;
	}
	
	public Payment findByOrderId(String rrn) {
		Payment smsRecord = (Payment) queryForObject(dsMnMainQry,"SELECT * FROM payment where rrn = ?",new Object[]{rrn}, Payment.class);
		return smsRecord;
	}
	
	public Payment add(final Payment pay) {
		final String sql = 
				"INSERT INTO payment("
				+"MSG_TYPE, PCODE, CID, COID, RRN, "
				+"CUID, PAID, AMOUNT, ERPC, ORDER_TYPE, "
				+"PAY_STATUS, RCODE, PAY_RCODE, ERP_ID, MID, "
				+"BID, MEMO, PRODUCT_NAME, PRODUCT_ID, USER_ACCTID, "
				+"USER_IP, TXTIME, EXTENSION, GPS_INFO, USER_GROUPID, "
				+"RMSG, RMSG_CHI) values "+
				"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		return (Payment) addForObject(dsMnMainUpd, sql, pay, new Object[] { 
				pay.getMSG_TYPE(), pay.getPCODE(), pay.getCID(), pay.getCOID(),
				pay.getRRN(), pay.getCUID(), pay.getPAID(), pay.getAMOUNT(),
				pay.getERPC(), pay.getORDER_TYPE(), pay.getPAY_STATUS(), pay.getRCODE(),
				pay.getPAY_RCODE(), pay.getERP_IP(), pay.getMID(), pay.getBID(),
				pay.getMEMO(), pay.getPRODUCT_NAME(), pay.getPRODUCT_ID(), pay.getUSER_ACCTID(),
				pay.getUSER_IP(), pay.getTXTIME(), pay.getExtension(), pay.getGPS_INFO(), pay.getUSER_GROUPID(),
				pay.getRMSG(), pay.getRMSG_CHI()
		});
	}

	public int update(final Payment pay) {
		final String sql = 
				"UPDATE payment SET "
				+"PCODE = ?, CID = ?, COID = ?, CUID = ?, "
				+"PAID = ?, AMOUNT = ?, ERPC = ?, ORDER_TYPE = ?, "
				+"PAY_STATUS = ?, RCODE = ?, PAY_RCODE = ?, ERP_ID = ?, MID = ?, "
				+"BID = ?, MEMO = ?, PRODUCT_NAME = ?, PRODUCT_ID = ?, USER_ACCTID = ?, "
				+"USER_IP = ?, TXTIME = ?, EXTENSION = ?, GPS_INFO = ?, USER_GROUPID = ?, "
				+"RMSG = ?, RMSG_CHI = ? WHERE RRN=?";
		
		return updateForObject(dsMnMainUpd, sql, new Object[] { 
				pay.getPCODE(), pay.getCID(), pay.getCOID(), pay.getCUID(), 
				pay.getPAID(), pay.getAMOUNT(), pay.getERPC(), pay.getORDER_TYPE(), 
				pay.getPAY_STATUS(), pay.getRCODE(), pay.getPAY_RCODE(), pay.getERP_IP(), pay.getMID(),
				pay.getBID(), pay.getMEMO(), pay.getPRODUCT_NAME(), pay.getPRODUCT_ID(), pay.getUSER_ACCTID(),
				pay.getUSER_IP(), pay.getTXTIME(), pay.getExtension(), pay.getGPS_INFO(), pay.getUSER_GROUPID(),
				pay.getRMSG(), pay.getRMSG_CHI(), pay.getRRN() 
		});
	}
}
