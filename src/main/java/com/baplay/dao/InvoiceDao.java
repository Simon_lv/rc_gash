package com.baplay.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.baplay.dto.Invoice;

@Repository
public class InvoiceDao extends BaseDao{
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public InvoiceDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}

	public Invoice add(Invoice invoice) {
		final String sql = 
				"INSERT INTO `rcinvoice` (`order_id`, `uid`, `acc_name`, `acc_no`, `invoice_type`, `email`, "
				+ "`invoice_name`, `invoice_phone`, `invoice_address`, `invoice_title`, `tax_id`, "
				+ "`create_time`) VALUES "
				+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		return (Invoice) addForObject(dsMnMainUpd, sql, invoice, new Object[] { 
				invoice.getOrderId(), invoice.getUid(), invoice.getAccName(), invoice.getAccNo(), invoice.getInvoiceType(), invoice.getEmail(), 
				invoice.getInvoiceName(), invoice.getInvoicePhone(), invoice.getInvoiceAddress(), invoice.getInvoiceTitle(), 
				invoice.getTaxId(), invoice.getCreateTime()});
	}

}
