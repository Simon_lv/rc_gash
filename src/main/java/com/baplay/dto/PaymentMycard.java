package com.baplay.dto;

import java.sql.Timestamp;

/*
* File		: Payment.java
* Date Created	: Thu Jun 11 09:36:02 CST 2015
*/

public class PaymentMycard extends BaseDto {

	private String TradeSeq;
	private String FacTradeSeq;
	private String Currency;
	private int Amount;
	private String PaymentType;
	private String MyCardTradeNo;
	private String MyCardType;
	private String SerialId;
	private String PromoCode;
	private String ReturnCode;
	private String ReturnMsg;
	private String PayResult;
	private String AuthCode;
	private String CustomerId;
	private String FinishTime;
	public String getTradeSeq() {
		return TradeSeq;
	}
	public void setTradeSeq(String tradeSeq) {
		TradeSeq = tradeSeq;
	}
	public String getFacTradeSeq() {
		return FacTradeSeq;
	}
	public void setFacTradeSeq(String facTradeSeq) {
		FacTradeSeq = facTradeSeq;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public int getAmount() {
		return Amount;
	}
	public void setAmount(int amount) {
		Amount = amount;
	}
	public String getPaymentType() {
		return PaymentType;
	}
	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}
	public String getMyCardTradeNo() {
		return MyCardTradeNo;
	}
	public void setMyCardTradeNo(String myCardTradeNo) {
		MyCardTradeNo = myCardTradeNo;
	}
	public String getMyCardType() {
		return MyCardType;
	}
	public void setMyCardType(String myCardType) {
		MyCardType = myCardType;
	}
	public String getSerialId() {
		return SerialId;
	}
	public void setSerialId(String serialId) {
		SerialId = serialId;
	}
	public String getPromoCode() {
		return PromoCode;
	}
	public void setPromoCode(String promoCode) {
		PromoCode = promoCode;
	}
	public String getReturnCode() {
		return ReturnCode;
	}
	public void setReturnCode(String returnCode) {
		ReturnCode = returnCode;
	}
	public String getReturnMsg() {
		return ReturnMsg;
	}
	public void setReturnMsg(String returnMsg) {
		ReturnMsg = returnMsg;
	}
	public String getPayResult() {
		return PayResult;
	}
	public void setPayResult(String payResult) {
		PayResult = payResult;
	}
	public String getAuthCode() {
		return AuthCode;
	}
	public void setAuthCode(String authCode) {
		AuthCode = authCode;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getFinishTime() {
		return FinishTime;
	}
	public void setFinishTime(String finishTime) {
		FinishTime = finishTime;
	}
	
	@Override
	public String toString() {
		return "PaymentMycard [TradeSeq=" + TradeSeq + ", FacTradeSeq="
				+ FacTradeSeq + ", Currency=" + Currency + ", Amount=" + Amount
				+ ", PaymentType=" + PaymentType + ", MyCardTradeNo="
				+ MyCardTradeNo + ", MyCardType=" + MyCardType + ", SerialId="
				+ SerialId + ", PromoCode=" + PromoCode + ", ReturnCode="
				+ ReturnCode + ", ReturnMsg=" + ReturnMsg + ", PayResult="
				+ PayResult + ", AuthCode=" + AuthCode + ", CustomerId="
				+ CustomerId + ", FinishTime=" + FinishTime + "]";
	}
	
	
}