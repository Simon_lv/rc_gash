package com.baplay.dto;

/*
* File		: Payment.java
* Date Created	: Thu Jun 11 09:36:02 CST 2015
*/

public class Payment extends BaseDto {

	private String GPS_INFO;
	private String PRODUCT_ID;
	private String ERPC;
	private String RCODE;
	private String CID;
	private String MID;
	private String MEMO;
	private String USER_IP;
	private String CUID;
	private String ORDER_TYPE;
	private String PCODE;
	private String BID;
	private float AMOUNT;
	private String extension;
	private String RRN;
	private String PAY_RCODE;
	private String ERP_IP;
	private String USER_ACCTID;
	private String PAID;
	private String PRODUCT_NAME;
	private String MSG_TYPE;
	private String TXTIME;
	private String COID;
	private String PAY_STATUS;
	private String USER_GROUPID;
	private String RMSG;
	private String RMSG_CHI;
	

	public String getGPS_INFO() {
		return GPS_INFO;
	}

	public void setGPS_INFO(String gpsInfo) {
		this.GPS_INFO = gpsInfo;
	}

	public String getPRODUCT_ID() {
		return PRODUCT_ID;
	}

	public void setPRODUCT_ID(String productId) {
		this.PRODUCT_ID = productId;
	}

	public String getERPC() {
		return ERPC;
	}

	public void setERPC(String ERPC) {
		this.ERPC = ERPC;
	}

	public String getRCODE() {
		return RCODE;
	}

	public void setRCODE(String RCODE) {
		this.RCODE = RCODE;
	}

	public String getCID() {
		return CID;
	}

	public void setCID(String cid) {
		this.CID = cid;
	}

	public String getMID() {
		return MID;
	}

	public void setMID(String mid) {
		this.MID = mid;
	}

	public String getMEMO() {
		return MEMO;
	}

	public void setMEMO(String memo) {
		this.MEMO = memo;
	}

	public String getUSER_IP() {
		return USER_IP;
	}

	public void setUSER_IP(String userIp) {
		this.USER_IP = userIp;
	}

	public String getCUID() {
		return CUID;
	}

	public void setCUID(String cuid) {
		this.CUID = cuid;
	}

	public String getORDER_TYPE() {
		return ORDER_TYPE;
	}

	public void setORDER_TYPE(String orderType) {
		this.ORDER_TYPE = orderType;
	}

	public String getPCODE() {
		return PCODE;
	}

	public void setPCODE(String pcode) {
		this.PCODE = pcode;
	}

	public String getBID() {
		return BID;
	}

	public void setBID(String bid) {
		this.BID = bid;
	}

	public float getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(float amount) {
		this.AMOUNT = amount;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getRRN() {
		return RRN;
	}

	public void setRRN(String rrn) {
		this.RRN = rrn;
	}

	public String getPAY_RCODE() {
		return PAY_RCODE;
	}

	public void setPAY_RCODE(String payRCODE) {
		this.PAY_RCODE = payRCODE;
	}

	public String getERP_IP() {
		return ERP_IP;
	}

	public void setERP_IP(String erpId) {
		this.ERP_IP = erpId;
	}

	public String getUSER_ACCTID() {
		return USER_ACCTID;
	}

	public void setUSER_ACCTID(String userAcctid) {
		this.USER_ACCTID = userAcctid;
	}

	public String getPAID() {
		return PAID;
	}

	public void setPAID(String paid) {
		this.PAID = paid;
	}

	public String getPRODUCT_NAME() {
		return PRODUCT_NAME;
	}

	public void setPRODUCT_NAME(String productName) {
		this.PRODUCT_NAME = productName;
	}

	public String getMSG_TYPE() {
		return MSG_TYPE;
	}

	public void setMSG_TYPE(String MSG_TYPE) {
		this.MSG_TYPE = MSG_TYPE;
	}

	public String getTXTIME() {
		return TXTIME;
	}

	public void setTXTIME(String txtime) {
		this.TXTIME = txtime;
	}

	public String getCOID() {
		return COID;
	}

	public void setCOID(String coid) {
		this.COID = coid;
	}

	public String getPAY_STATUS() {
		return PAY_STATUS;
	}

	public void setPAY_STATUS(String payStatus) {
		this.PAY_STATUS = payStatus;
	}

	public String getUSER_GROUPID() {
		return USER_GROUPID;
	}

	public void setUSER_GROUPID(String uSER_GROUPID) {
		USER_GROUPID = uSER_GROUPID;
	}

	public String getRMSG() {
		return RMSG;
	}

	public void setRMSG(String rMSG) {
		RMSG = rMSG;
	}

	public String getRMSG_CHI() {
		return RMSG_CHI;
	}

	public void setRMSG_CHI(String rMSG_CHI) {
		RMSG_CHI = rMSG_CHI;
	}

	
}