package com.baplay.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.PaymentDao;
import com.baplay.dto.Payment;



/**
 * 儲值紀錄
 * @version 1.0
 * */
@Service
@Transactional
public class PaymentManager {
	@Autowired
    private PaymentDao paymentDao;
	
	/**
	 * 用儲值ID找儲值
	 * */
	public Payment findById(long id){
		return paymentDao.findById(id);
	}
	
	/**
	 * 儲存儲值
	 * */
	public Payment add(Payment pay){
		return paymentDao.add(pay);
	}
	
	/**
	 * 用儲值RRN找儲值
	 * */
	public Payment findByOrderId(String rrn){
		return paymentDao.findByOrderId(rrn);
	}
	
	/**
	 * 儲存儲值
	 * */
	public int update(Payment pay){
		return paymentDao.update(pay);
	}
	
}
