package com.baplay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.InvoiceDao;
import com.baplay.dto.Invoice;

@Service
@Transactional
public class InvoiceManager {
	@Autowired
	private InvoiceDao invoiceDao;
	
	public Invoice add(Invoice invoice) {
		return invoiceDao.add(invoice);
	}

}
