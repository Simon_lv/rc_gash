package com.baplay.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.PaymentMycardDao;
import com.baplay.dto.PaymentMycard;



/**
 * 儲值紀錄
 * @version 1.0
 * */
@Service
@Transactional
public class PaymentMycardManager {
	@Autowired
    private PaymentMycardDao paymentMycardDao;
	
	/**
	 * 用儲值ID找儲值
	 * */
	public PaymentMycard findById(long id){
		return paymentMycardDao.findById(id);
	}
	
	/**
	 * 儲存儲值
	 * */
	public PaymentMycard add(PaymentMycard pay){
		return paymentMycardDao.add(pay);
	}
	
	/**
	 * 用儲值RRN找儲值
	 * */
	public PaymentMycard findByOrderId(String tradeSeq){
		return paymentMycardDao.findByOrderId(tradeSeq);
	}
	
	/**
	 * 儲存儲值
	 * */
	public int update(PaymentMycard pay){
		return paymentMycardDao.update(pay);
	}
	
}
